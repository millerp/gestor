<?php
$app_strings = Array(
    'pt_br' => "Português",
    'customerportal' => 'Portal do Cliente',
    'LBL_LOGIN' => 'Login',
    'LBL_USER_NAME' => 'Email',
    'LBL_PASSWORD' => 'Senha',
    'LNK_HOME' => 'Home',
    'LBL_WELCOME' => 'Bem-vindo',
    'LNK_LOGOUT' => 'Sair',
    'Close' => 'Fechar',
    'LBL_RPASSWORD' => 'Recuperar Senha',
    'Login' => 'Entrar',
    'LBL_FPASSWORD' => 'Esqueceu Sua Senha?',
    'LBL_SREQUEST' => 'Enviar',
    'LBL_BACK_LOGIN' => 'Voltar para o Login',
    'Subject' => 'Assunto',
    'Urgent' => 'Urgente',
    'High' => 'Alto',
    'Low' => 'Baixo',
    'Minor' => 'Menor',
    'Major' => 'Maior',
    'Feature' => 'Caracteristica',
    'Critical' => 'Critico',
    'Big Problem' => 'Problema Grande',
    'Small Problem' => 'Problema Pequeno',
    'Other Problem' => 'Outro Problema',
    'UPLOAD_COMPLETED' => 'Arquivo enviado com sucesso!', 
    'Project Related Tickets' => 'Chamados Relacionados',
    'Project Progress' => 'Andamento do Projeto',
    'The record could not be found!' => 'O registro não pode ser encontrado!',
    
    // Menu
    'Home' => 'Home',
    'HelpDesk' => 'Chamados',
    'Submit Ticket' => 'Enviar Chamado',
    'Ticket Status' => 'Status do Chamado',
    'Close Ticket' => 'Fechar Chamado',
    'Attachments' => 'Arquivos',
    'Ticket Comments' => 'Comentários',
    'Send Comment' => 'Enviar Comentário',
    'Faq' => 'Faq',
    'Documents' => 'Documentos',
    'LBL_NOTES_INFORMATION' => 'Documentos',
    'Contacts' => 'Contatos',
    'Accounts' => 'Contas',
    'Project' => 'Projetos',
    'New Ticket' => 'Novo Chamado',
    'No Home records Found!' => 'Nenhum registro encontrado!',
    'No HelpDesk records Found!' => 'Nenhum registro encontrado!',
    'No Faq records Found!' => 'Nenhum registro encontrado!',
    'No Documents records Found!' => 'Nenhum registro encontrado!',
    'No Contacts records Found!' => 'Nenhum registro encontrado!',
    'No Accounts records Found!' => 'Nenhum registro encontrado!',
    'No Project records Found!' => 'Nenhum registro encontrado!',
    'View All' => 'Ver Todos',
    'Open' => 'Aberto',
    'In Progress' => 'Em Andamento',
    'Closed' => 'Fechado',
    'Wait For Response' => 'Aguardando Resposta',
    //Tickets Language Strings
    'LBL_NEW_TICKET' => 'Novo Chamaddo',
    'LBL_MY_OPEN_TICKETS' => 'Meus Chamados Abertos',
    'LBL_CLOSED_TICKETS' => 'Chamados Fechados',
    'TICKETID' => 'ID',
    'TICKET_TITLE' => 'Titulo',
    'TICKET_PRIORITY' => 'Prioridade',
    'TICKET_STATUS' => 'Status',
    'TICKET_CATEGORY' => 'Categoria',
    'TICKET_MATCH' => 'Match',
    'LBL_NONE_SUBMITTED' => 'Nenhum chamado enviado',
    'LBL_CREATE_NEW_TICKET' => 'Criar novo Chamado',
    'LBL_PRODUCT_NAME' => 'Nome do Produto',
    'LBL_SERVICE_CONTRACTS' => 'Contratos de Serviço',
    'LBL_TICKET_PRIORITY' => 'Prioridade do Chamado',
    'LBL_TICKET_SEVERITY' => 'Severidade do Chamado',
    'LBL_TICKET_CATEGORY' => 'Categoria do Chamado',
    'LBL_DESCRIPTION' => 'Descrição',
    'LBL_CLOSE_TICKET' => 'Fechar este Chamado',
    'LBL_COMMENT_BY' => 'Comentário por',
    'NO_ATTACHMENTS' => 'Não há arquivos disponíveis',
    'LBL_FILE_UPLOADERROR' => 'Erro ao enviar arquivo:',
    'LBL_ATTACH_FILE' => 'Anexar Arquivo :',
    'LBL_ATTACH' => 'Anexo',
    'NONE' => 'Nenhum',
    'LBL_SEARCH' => 'Pesquisar',
    'LBL_TICKETS' => 'Chamados',
    'LBL_STATUS_CLOSED' => 'Fechado', //Do not convert this label. This is used to check the status. If the status 'Closed' is changed in vtigerCRM server side then you will give the exact value of status 'Closed' which is in vtigerCRM server.
    'LBL_NEW_INFORMATION' => 'Por favor, insira as informações necessárias nos seguintes campos para enviar um chamado.',
    'LBL_TICKET_ID' => 'ID',
    'LBL_STATUS' => 'Status',
    'LBL_ON' => 'on',
    'LBL_NOTSET_UPLOAD_DIR' => 'Você não definiu diretório de carregamento em config ARQUIVO',
    'LBL_GIVE_VALID_FILE' => 'Por favor, dê um arquivo válido para upload!',
    'LBL_UPLOAD_FILE_LARGE' => 'Desculpe, o arquivo enviado excede o limite máximo de tamanho de arquivo. Por favor, tente um pequeno arquivo',
    'LBL_PROBLEM_UPLOAD' => 'Problemas no upload de arquivos. Por favor, tente novamente!',
    'LBL_FILE_HAS_NO_CONTENTS' => 'O arquivo não tem conteúdo, portanto, não pode fazer o upload do arquivo',
    'LBL_UPLOAD_VALID_FILE' => 'Faça upload de um arquivo válido.',
    'LBL_PROBLEM_IN_TICKET_SAVING' => '<br> Pode haver problema em salvar o Chamado. Por favor verifique se o Chamado foi criado ou não',
    'LBL_ALL' => 'Todos',
    'LBL_ANY' => 'Qualquer',
    //Tickets Block Name
    'Ticket Information' => 'Informações do Chamado',
    'Description Information' => 'Descrição',
    'Solution Information' => 'Solução',
    'LBL_TICKET_COMMENTS' => 'Comentários',
    'LBL_ADD_COMMENT' => 'Adicionar comentário',
    'LBL_ATTACHMENTS' => 'Arquivos',
    //Tickets Fields
    'Title' => 'Titulo',
    'Assigned To' => 'Atribuído a',
    'Priority' => 'Prioridade',
    'Severity' => 'Severidade',
    'Hours' => 'Horas',
    'Days' => 'Dias',
    'Ticket No' => 'Chamado NO',
    'Related to' => 'Nome da Organização',
    'Product Name' => 'nome do Produto',
    'Status' => 'Status',
    'Category' => 'Categoria',
    'Description' => 'Descrição',
    'Solution' => 'Solução',
    //My settings
    'LBL_CHANGE_PASSWORD' => 'Alterar Senha',
    'Change Password' => 'Alterar Senha',
    'Old Password' => 'Senha Antiga',
    'New Password' => 'Nova Senha',
    'Confirm New Password' => 'Confirme senha',
    'LBL_MY_DETAILS' => 'Meus Detalhes',
    'LBL_LAST_LOGIN' => 'Ultimo login',
    'LBL_SUPPORT_START_DATE' => 'Suporte Data de Início',
    'LBL_SUPPORT_END_DATE' => 'Suporte Data de Término',
    'MSG_PASSWORD_CHANGED' => 'A senha foi alterada com sucesso.',
    'MSG_ENTER_NEW_PASSWORDS_SAME' => 'As senhas não conferem.',
    'MSG_YOUR_PASSWORD_WRONG' => 'Senha antiga incorreta.',
    //Added fields for Knowledge Base details
    'LBL_KNOWLEDGE_BASE' => 'Base de Conhecimento',
    'LBL_COMMENTS' => 'Comentários',
    'LNK_CATEGORY' => 'Categoria',
    'LNK_PRODUCTS' => 'Produtos',
    'LBL_SEARCH_RESULT' => 'Resultado da Pesquisa:',
    'LBL_NO_FAQ_IN_THIS_CATEGORY' => 'Não há artigos nesta categoria.',
    'LBL_NO_FAQ_IN_THIS_PRODUCT' => 'Não há artigos nesse Produto.',
    'LBL_NO_FAQ' => 'Não há artigos.',
    'LBL_NO_FAQ_IN_THIS_SEARCH_CRITERIA' => 'Não há artigos combinados com os dados critérios de pesquisa.',
    'LBL_PRODUCT' => 'Produto',
    'LBL_ADDED_ON' => 'Adicionado em: ',
    'LBL_FAQ_ID' => 'Faq Id',
    'LBL_PRINT_THIS_PAGE' => 'Imprima esta página',
    'LBL_EMAIL_THIS_PAGE' => 'Envie esta página',
    'LBL_ADD_TO_FAVORITES' => 'Adicionar a Favoritos',
    'LBL_RECENTLY_CREATED' => 'Adicionados Recentemente',
    'LBL_CREATED_DATE' => 'Data de Criação',
    'LBL_MODIFIED_DATE' => 'Data de Modificação',
    'KBASE_DETAILS' => 'A base de conhecimento é organizado com base em ambas as categorias e produtos, por favor, selecione uma categoria ou produto que você está interessado.
Além disso, você também pode pesquisar toda a base de conhecimento, digitando palavras-chave abaixo.',
    'LBL_DOCUMENTS' => 'Documentos relacionados',
    'LBL_FAQ_TITLE' => 'Knowledge Base Título',
    'LBL_FAQ_DETAIL' => 'Knowledge Base Detalhe',
    'LBL_ARTICLE_INTERESTED' => 'Aqui está um artigo que você pode estar interessado em:',
    'LBL_PRESS_CNTR_D' => 'Pressione Ctrl+D',
    //Contacts
    'Contact Information' => 'Informações De Contato',
    'Customer Portal Information' => 'Cliente Portal de Informação',
    'Address Information' => 'Informações de Endereço',
    'Description Information' => 'Descrição',
    'Custom Information' => 'Informação Customizada',
    //User
    'First Name' => 'Primeiro Nome',
    'Contact Id' => 'Contato Id',
    'Office Phone' => 'Telefone Escritório',
    'Last Name' => 'Sobrenome',
    'Mobile' => 'Celular',
    'Lead Source' => 'Lead Source',
    'Home Phone' => 'Telefone Residencial',
    'Other Phone' => 'Outro telefone',
    'Department' => 'Departamento',
    'Fax' => 'Fax',
    'Email' => 'Email',
    'Mailing Street' => 'Endereço',
    'Mailing Po Box' => 'Caixa Postal',
    'Mailing City' => 'Cidade',
    'Mailing State' => 'Estado',
    'Mailing Postal Code' => 'CEP',
    'Mailing Country' => 'Páis',
    'Mailing Zip' => 'Mailing Zip',
    'Other Zip' => 'Other Zip',
    'Other Street' => 'Other Street',
    'Other Po Box' => 'Other PO Box',
    'Other City' => 'Other City',
    'Other State' => 'Other State',
    'Other Postal Code' => 'Other Postal Code',
    'Other Country' => 'Other Country',
    'Description Information' => 'Descrição',
    'LBL_CONTACTS' => 'Contatos',
    'Assistant' => 'Assistente',
    'Birthdate' => 'Data De Nascimento',
    'Assistant Phone' => 'Asistente de telefone',
    'Reports To' => 'rRlatórios para',
    'Yahoo Id' => 'Yahoo Id',
    'Do Not Call' => 'Não chame',
    'Reference' => 'Referência',
    'Support End Date' => 'Suporte Data de Término',
    //products
    'LBL_PRODUCT_INFORMATION' => 'Produtos ',
    //Product Block Name
    'Product Information' => 'Informações do Produto',
    'Pricing Information:' => 'Informações do Preço:',
    'Stock Information:' => 'Informações sobre estoque:',
    'Description Information' => 'Descrição',
    //Products Fields
    'Product Name' => 'Nome do Produto',
    'Product Active' => 'Produto Ativo',
    'Sales Start Date' => 'Vendas Data de início',
    'Product Category' => 'Categoria do Produto',
    'Sales End Date' => 'Data final de Vendas',
    'Vendor Name' => 'Nome do fornecedor',
    'Vendor PartNo' => 'Vendor PartNo',
    'Product Sheet' => 'Folha de produto',
    'Product No' => 'Produto No',
    'Part Number' => 'Part Number',
    'Manufacturer' => 'Fabricante',
    'Support Start Date' => 'Suporte Data de Início',
    'Support Expiry Date' => 'Suporte Data de Validade',
    'Website' => 'Website',
    'Mfr PartNo' => 'Mfr PartNo',
    'Serial No' => 'Serial No',
    'GL Account' => 'GL Account',
    'Unit Price' => 'Preço Unitário',
    'Commission Rate' => 'Comissão Taxa',
    'Usage Unit' => 'Unidade de uso',
    'Qty/Unit' => 'Qty/Unit',
    'Qty In Stock' => 'Qty em Estoque',
    'Reorder Level' => 'Reordenar Nível',
    'Handler' => 'Treinador',
    'Qty In Demand' => 'Qty em Demanda',
    'Description' => 'Descrição',
    //Quotes
    'LBL_QUOTE_INFORMATION' => 'Cotações',
    //Quotes Block Name
    'Quote Information' => 'Orçamento Informação',
    'Address Information' => 'Informações de Endereço',
    'Terms & Conditions' => 'Termos & Condições',
    'Description Information' => 'Descrição Informação',
    //Quotes Fields
    '(Download PDF) Subject' => '(Download PDF) Assunto---',
    'Potential Name' => 'Nome de Oportunidades',
    'Quote No' => 'Cotação NO',
    'Quote Stage' => 'Citar fase',
    'Valid Till' => 'Válido até',
    'Contact Name' => 'Nome do contato',
    'Carrier' => 'Portador',
    'Shipping' => 'Remessa',
    'Inventory Manager' => 'Gerente de estoque',
    'Account Name' => 'Nome da Organização',
    'Billing Address' => 'Endereço De Cobrança',
    'Billing Po Box' => 'Faturamento Po Box',
    'Billing City' => 'Faturamento da Cidade',
    'Billing State' => 'Estado Billing',
    'Billing Code' => 'Código de faturamento',
    'Billing Country' => 'Faturamento País ',
    'Shipping Address' => 'Endereço Para Envio',
    'Shipping Po Box' => 'O envio Po Box',
    'Shipping City' => 'O envio da Cidade',
    'Shipping State' => 'Estado envio',
    'Shipping Code' => 'Código da Marinha',
    'Shipping Country' => 'O envio País',
    'Terms & Conditions' => 'Termos & Condições',
    'Description' => 'Descrição',
    'LBL_PDF_CANNOT_GENERATE' => 'Não é possível gerar PDF',
);
