<?php
/* * *******************************************************************************
 * The content of this file is subject to the MYC Vtiger Customer Portal license.
 * ("License"); You may not use this file except in compliance with the License
 * The Initial Developer of the Original Code is Proseguo s.l. - MakeYourCloud
 * Portions created by Proseguo s.l. - MakeYourCloud are Copyright(C) Proseguo s.l. - MakeYourCloud
 * All Rights Reserved.
 * ****************************************************************************** */
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo Language::translate($module); ?></h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">

        <?php
        if (isset($data['recordinfo']) && count($data['recordinfo']) > 0 && $data['recordinfo'] != "") {

            foreach ($data['recordinfo'] as $blockname => $tblocks):
                ?>
                <div class="col-lg-6">


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo Language::translate($blockname); ?>
                        </div>


                        <table class="table">
                            <?php
                            foreach ($tblocks as $field) {

                                if (DateTime::createFromFormat('Y-m-d', substr($field['value'], 0, 10)) !== FALSE) {
                                    echo "<tr><td><b>" . Language::translate($field['label']) . ": </b></td><td>" . Jenssegers\Date\Date::createFromFormat("Y-m-d", substr($field['value'], 0, 10))->format('d/m/Y') . "</td></tr>";
                                } else {
                                    echo "<tr><td><b>" . Language::translate($field['label']) . ": </b></td><td>" . Language::translate($field['value']) . "</td></tr>";
                                }
                            }
                            ?>

                        </table>



                    </div>
                    <!-- /.panel -->

                </div>
                <!-- /.col-lg-6 -->

            <?php endforeach; ?>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo Language::translate("Project Progress"); ?>
                    </div>
                    <div class="table-responsive">

                        <div id="chart_projeto" style="width: 100%; height: 350px;"></div> 
                    </div>
                </div>
            </div>
            <div class="col-lg-12">


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <?php echo Language::translate("Project Related Tickets"); ?>
                        <div class="input-group pull-right">
                            <a href="index.php?module=HelpDesk&action=new&projectid=<?php echo $data['recordid']; ?>" class="btn btn-warning btn-sm pull-right"><?php echo Language::translate("New Ticket"); ?></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>   

                    <?php if (isset($data['relatedticketlist']) && count($data['relatedticketlist']) > 0 && $data['relatedticketlist'] != ""): ?>  

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables" >
                                <thead>
                                    <tr>
                                        <?php
                                        foreach ($data['relatedtickettableheader'] as $hf)
                                            echo "<th>" . Language::translate($hf['fielddata']) . "</th>";
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    foreach ($data['relatedticketlist'] as $record) {

                                        echo "<tr>";
                                        foreach ($record as $record_fields)
                                            echo "<td>" . Language::translate($record_fields['fielddata']) . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>

                    <?php endif; ?>
                </div>
            </div>



            <?php if (isset($data['relatedtaskslist']) && count($data['relatedtaskslist']) > 0 && $data['relatedtaskslist'] != ""): ?>  
                <div class="col-lg-12">


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo Language::translate("Project Related Tickets"); ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables" >
                                <thead>
                                    <tr>
                                        <?php
                                        foreach ($data['relatedtaskstableheader'] as $hf)
                                            echo "<th>" . Language::translate($hf['fielddata']) . "</th>";
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    foreach ($data['relatedtaskslist'] as $record) {

                                        echo "<tr>";
                                        foreach ($record as $record_fields)
                                            echo "<td>" . Language::translate($record_fields['fielddata']) . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if (isset($data['relatedmilestoneslist']) && count($data['relatedmilestoneslist']) > 0 && $data['relatedmilestoneslist'] != ""): ?>  
                <div class="col-lg-12">


                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo Language::translate("Project Related Milestones"); ?>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover dataTables" >
                                <thead>
                                    <tr>
                                        <?php
                                        foreach ($data['relatedmilestonestableheader'] as $hf)
                                            echo "<th>" . Language::translate($hf['fielddata']) . "</th>";
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    foreach ($data['relatedmilestoneslist'] as $record) {

                                        echo "<tr>";
                                        foreach ($record as $record_fields)
                                            echo "<td>" . Language::translate($record_fields['fielddata']) . "</td>";
                                        echo "</tr>";
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            <?php endif; ?>

        <?php } else echo "<div class='col-lg-12'><h2>" . Language::translate("The record could not be found!") . "</h2></div>"; ?>



    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->

<?php
// Dados do chart
$design = intval(substr($data['projeto'][0]['cf_765'] / $data['projeto'][0]['cf_769'] * 100, 0, 3));
$analise = intval(substr($data['projeto'][0]['cf_807'] / $data['projeto'][0]['cf_805'] * 100, 0, 3));
$frontend = intval(substr($data['projeto'][0]['cf_773'] / $data['projeto'][0]['cf_787'] * 100, 0, 3));
$backend = intval(substr($data['projeto'][0]['cf_775'] / $data['projeto'][0]['cf_785'] * 100, 0, 3));
$conteudo = intval(substr($data['projeto'][0]['cf_767'] / $data['projeto'][0]['cf_771'] * 100, 0, 3));
$teste = intval(substr($data['projeto'][0]['cf_777'] / $data['projeto'][0]['cf_789'] * 100, 0, 3));
$gerencia = intval(substr($data['projeto'][0]['cf_803'] / $data['projeto'][0]['cf_801'] * 100, 0, 3));
?>
<script>
    $(document).ready(function () {
        $('.dataTables').dataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/f2c75b7247b/i18n/Portuguese-Brasil.json"
            }
        });
    });

    google.load('visualization', '1', {packages: ['corechart']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Segmento', 'Porcentagem(%)'],
            ['Design', <?php echo $design > 100 ? 110 : $design; ?>],
            ['Analise', <?php echo $analise > 100 ? 110 : $analise; ?>],
            ['Frontend', <?php echo $frontend >  100 ? 110 : $frontend; ?>],
            ['Backend', <?php echo $backend > 100 ? 110 : $backend; ?>],
            ['Conteudo', <?php echo $conteudo > 100 ? 110 : $conteudo; ?>],
            ['Testes', <?php echo $teste > 100 ? 110 : $teste; ?>],
            ['Gerencia', <?php echo $gerencia > 100 ? 110 : $gerencia; ?>]
        ]);

        var options = {
             legend: { position: 'top', maxLines: 3 },
            //title: 'Population of Largest U.S. Cities',
            //width: 1000,
            //height: 563,
            hAxis: {
                title: 'Segmento',
                minValue: 0,
                maxValue: 110
            },
            vAxis: {
                title: 'Progresso'
            }
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_projeto'));

        chart.draw(data, options);
    }

</script>

