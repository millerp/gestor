<?php return array (
  'admin_user' => 'admin',
  'admin_pass' => 'admin',
  'admin_email' => 'miller@agnecia110.com.br',
  'vtiger_path' => 'http://gestor.agencia110.com.br',
  'upload_dir' => '/tmp',
  'default_charset' => 'UTF-8',
  'default_timezone' => 'America/Sao_Paulo',
  'default_language' => 'pt_br',
  'hiddenmodules' => 
  array (
    0 => 'ProjectTask',
    1 => 'ProjectMilestone',
  ),
  'show_dashboard' => 'true',
  'portal_theme' => 'default',
  'api_user' => '',
  'api_pass' => '',
  'enabled_api_modules' => 
  array (
    0 => 'Events',
  ),
  'google_api_key' => '',
  'portal_logo' => 'logo_110.png',
) ;
