<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.2
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

/**
 * Function to check values in array regardless of case
 */
function in_array_case_insensitive($needle, $haystack) 
{
	global $log;
	$log->debug("Entering in_array_case_insensitive($needle haystack array()) method....");
	$log->debug("Exiting in_array_case_insensitive($needle haystack array()) method....");
	return in_array( strtolower($needle), array_map('strtolower', $haystack) );
}

/**
 * Function to santize and check valid module name.
 */
function sanitizeModuleName($module)
{
	global $log;
	
	$log->debug("Entering sanitizeModuleName($module) method....");
		
	$func  = get_defined_functions();
	$vars  = get_defined_vars();
	$cls   = get_declared_classes();
	$keywords = array('__halt_compiler', 'abstract', 'and', 'array',
					 'as', 'break', 'callable', 'case', 'catch', 
					 'class', 'clone', 'const', 'continue', 'declare', 
					 'default', 'die', 'do', 'echo', 'else', 
					 'elseif', 'empty', 'enddeclare', 'endfor', 
					 'endforeach', 'endif', 'endswitch', 'endwhile', 
					 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 
					 'function', 'global', 'goto', 'if', 'implements', 'include', 
					 'include_once', 'instanceof', 'insteadof', 
					 'interface', 'isset', 'list', 'namespace', 'new', 'or',
					 'print', 'private', 'protected', 'public', 'require', 
					 'require_once', 'return', 'static', 'switch', 'throw', 
					 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor');

	$predefined_constants = array('__CLASS__', '__DIR__', '__FILE__', 
								  '__FUNCTION__', '__LINE__', '__METHOD__', 
								  '__NAMESPACE__', '__TRAIT__');
	
	if( in_array_case_insensitive( $module, $func['internal'] ) || in_array_case_insensitive( $module, $vars['func']['internal']) 
		|| in_array_case_insensitive( $module, $cls ) || in_array_case_insensitive( $module, $keywords ) 
		|| in_array_case_insensitive ( $module, $predefined_constants ) || function_exists( $module) 
		|| class_exists( $module) )
	{
		$log->debug("Exiting sanitizeModuleName($module) method.... MODULE NAME IS A KEYWORD");
		return true;
	}	
	$log->debug("Exiting sanitizeModuleName($module) method.... MODULE NAME IS UINQUE");
	return false;
}
?>