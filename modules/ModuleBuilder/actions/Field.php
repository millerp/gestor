<?php
/***********************************************************************************************
** The contents of this file are subject to the Vtiger Module-Builder License Version 1.2
 * ( "License" ); You may not use this file except in compliance with the License
 * The Original Code is:  Technokrafts Labs Pvt Ltd
 * The Initial Developer of the Original Code is Technokrafts Labs Pvt Ltd.
 * Portions created by Technokrafts Labs Pvt Ltd are Copyright ( C ) Technokrafts Labs Pvt Ltd.
 * All Rights Reserved.
**
*************************************************************************************************/

class ModuleBuilder_Field_Action extends Settings_Vtiger_Index_Action 
{
	/**
	 * Class Constructor
	 */
    function __construct() 
	{
		global $log;
		$log->debug("Entering __construct() method....");
        $this->exposeMethod( 'add' );
        $this->exposeMethod( 'save' );
        $this->exposeMethod( 'delete' );
        $this->exposeMethod( 'move' );
        $this->exposeMethod( 'unHide' );
		$log->debug("Exiting __construct() method....");
    }
	
	/**
	 * Function to check permission for the User
	 */
	function checkPermission(Vtiger_Request $request) {
		global $log;
		$log->debug("Entering checkPermission() method....");
		$log->debug("Exiting checkPermission() method....");
		return true;
	}
	
	/**
	 * Function to check duplicate field for current module
	 */
	public function checkDuplicate( $req,$token ) 
	{
		global $log;
		$log->debug("Entering checkDuplicate($req,token) method....");
		if( isset( $_SESSION['tks_module_builder'][$token]['tks_blocks'] ) )
		{
			
			foreach( $_SESSION['tks_module_builder'][$token]['tks_blocks'] as $key => $value )
			{
				if(is_array($value['tks_fields']))
				{
					foreach( $value['tks_fields'] as $k => $v )
					{
						
						if( strcasecmp( $req, $v['tkslabel'] ) == 0 && $v['deleted'] == 'false' )
						{
							$log->debug("Exiting checkDuplicate($req,token) method.... DUPLICATE ENTRY EXSIST");
							return true;
						}
					}
				}
			}
		}
		$log->debug("Exiting checkDuplicate($req,token) method....");
		return false;
	}
	
	/**
	 * Function to get Boolean Value
	 */
	private function getBool( $val )
	{
		global $log;
		$log->debug("Entering getBool($val) method....");
		
		if ( $val == 'M' )
		{
			$log->debug("Exiting getBool($val) method....");
			return 'true'; 
		}	
			
		if ( $val == '1' )
		{
			$log->debug("Exiting getBool($val) method....");
			return 'true'; 
		}	
		else
		{
			$log->debug("Exiting getBool($val) method....");
			return 'false';	
		}	
	}
	
	/**
	 * Function to add field
	 */
    public function add( Vtiger_Request $request ) 
	{
		global $log;
		$log->debug("Entering add(request array()) method....");
		$token = $request->get('token');
		if($token == '')
		{
			$response = new Vtiger_Response();
			$response->setError('502', vtranslate('LBL_FAILED_MODULE_INIT', $request->getModule(false)));
			$response->emit();
			$log->debug("Exiting add() method.... INITIALIZATION SESSION ERROR");
			return;
		}
		
		$picklistvalues = array();
		if($request->get( 'pickListValues' ) != '')
		{
			$p = explode(',',$request->get( 'pickListValues' ));
			foreach($p as $key => $value)
			{
				$picklistvalues[$value] = $value;
			}
			
		}
		else
		{
			$picklistvalues = '';
		}

        $type 		= strtolower($request->get( 'fieldType' ));
        $moduleName = $request->get( 'sourceModule' );
        $blockId 	= $request->get( 'blockid' );
		$blockId 	= $blockId - 1;
		$fld 		= array();
       
	    $isDuplicate = $this -> checkDuplicate( $request->get( 'fieldLabel' ), $token );
        $response = new Vtiger_Response();
		if ( !$isDuplicate ) 
		{
        	try
			{
				$fld['tkslabel'] 		  	= $request->get( 'fieldLabel' );
				$fld['tkslength']		  	= $request->get( 'fieldLength' );
				$fld['tksdecimal']		  	= $request->get( 'decimal' );
				$fld['tkspicklistvalues'] 	= $request->get( 'pickListValues' );
				$fld['tksrelate'] 			= $request->get( 'relate' );
				$fld['tksrequired'] 	  	= $this->getBool( $request->get( 'isMandatory' ) );
				$fld['tksfilterfield'] 	 	= $this->getBool( $request->get( 'isFilter' ) );
				
				
				$_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockId]['tks_fields'][] = array( 0 => $fld['tkslabel'] );
				$end_val =  end( $_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockId]['tks_fields'] );
			
				$k = key( $_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockId]['tks_fields'] ); 
			
				$responseData = array(
						'id'				=> $k, 										 'blockid'				 	=> $blockId+1, 			
						'defaultvalue'		=> 'false', 								 'isQuickCreateDisabled'  	=> $this->getBool($request->get('isMandatory')),
						'isSummaryField' 	=> 'false', 								 'isSummaryFieldDisabled' 	=> 'false',
						'label'				=> $request->get('fieldLabel'),				 'mandatory'				=> $this->getBool($request->get('isMandatory')),
						'masseditable'		=> 'false',									 'name'					 	=> 'tks_'.str_replace(' ','',$request->get('fieldLabel')),
						'presence'			=> 'true',									 'quickcreate'			 	=> $this->getBool($request->get('isMandatory')),
						'type'				=> $type,									 'tksrequired' 	  		 	=> $this->getBool($request->get('isMandatory')),
						'tksfilterfield' 	=> $this->getBool($request->get( 'isFilter')),'customField'			 	=> 'true',
						'deleted' 			=> 'false',									 
					);
					
				if($picklistvalues != '')
				{
					$responseData['picklistvalues']	= $picklistvalues;
				}
				
				switch ($responseData['type']) 
				{
					case 'text':
							$responseData['type'] = 'string';
							break;
					
					case 'decimal':
							$responseData['type'] = 'double';
							break;
									
					case 'checkbox':
							$responseData['type'] = 'boolean';
							break;
					case 'multiselectcombo':
							$responseData['type'] = 'multipicklist';
							break;
					case 'percent':
							$responseData['type'] = 'percentage';
							break;
					case 'currency':
							$responseData['currency_symbol'] = '&#36;';
							break;
					case 'date':
							global $current_user;
							$responseData['date-format'] = $current_user->column_fields['date_format'];
							break;
					case 'time':
							global $current_user;
							$responseData['time-format'] = $current_user->column_fields['hour_format'];
							break;
					
				}
				
				$remove = array_pop( $_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockId]['tks_fields'] );
				foreach( $responseData as $key => $value )
				{
					$fld[$key] = $value;
				}	
				$fld['tksrelate'] = 	$request->get( 'relate' );
				$fld['tkspickListValues'] = 	$request->get( 'pickListValues' );
				$fld['tksdecimal'] = 	$request->get( 'decimal' );
					
				$_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockId]['tks_fields'][$k] = $fld;
				
				if($responseData['type'] == 'textarea')	
				{
					$responseData['type'] = 'string';
				}
				
            	$response->setResult( $responseData );
        	}
			catch( Exception $e ) 
			{
            	$response->setError( $e->getCode(), $e->getMessage() );
				$log->debug("add() method.... ERROR OCCURED");
        	}
		}
		else 
		{
			$response->setError( '502', vtranslate( 'LBL_DUPLICATE_FILED_EXIST', $request->getModule() ) );
			$log->debug("add() method.... ERROR OCCURED");
		}	
		$log->debug("Exiting add() method....");
        $response->emit();
    }
	
	/**
	 * Function to save field properties
	 */
    public function save( Vtiger_Request $request ) 
	{
		global $log;
		$log->debug("Entering save(request array()) method....");
		$token = $request->get('token');
		if($token == '')
		{
			$response = new Vtiger_Response();
			$response->setError('502', vtranslate('LBL_FAILED_MODULE_INIT', $request->getModule(false)));
			$response->emit();
			$log->debug("Exiting save(request array()) method.... INITIALIZATION SESSION ERROR");
			return;
		}
		
		$quickcreate = $request->get( 'quickcreate' );
		
		if($quickcreate == 0)
		{
			$quickcreate = 'M';	
		}	
		
		$presence = $request->get( 'presence' );
		if( $presence == '2' )
		{
			$presence = '1';
		}	
		else
		{
			$presence  = '0';
		}
		
		$defaultValue = $request->get('fieldDefaultValue');
		if(is_array($defaultValue)) {
			$defaultValue = implode(' |##| ',$defaultValue);
		}
		
        $fieldId = $request->get( 'fieldid' );
		$type = $_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['type'];
		if($type == 'date' && $defaultValue != '')
		{
			global $current_user;
			if($current_user->column_fields['date_format'] == 'dd-mm-yyyy')
			{
				$defaultValue = date('Y-m-d', strtotime($defaultValue));
			}
			else if($current_user->column_fields['date_format'] == 'mm-dd-yyyy')
			{
				$defaultValue = str_replace('-', '/', $defaultValue);
				$defaultValue = date('Y-m-d', strtotime($defaultValue));		
			}
		}
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['mandatory'] 	   
			= $this->getBool($request->get('mandatory'));
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['presence'] 		   
			= $this->getBool($presence);
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['quickcreate'] 	  
			= $this->getBool($quickcreate);
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['summaryfield'] 	   
			= $this->getBool($request->get('summaryfield'));
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['masseditable'] 	   
			= $this->getBool($request->get('masseditable'));
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['defaultvalue'] 	   
			= $this->getBool($request->get('defaultvalue'));
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['fieldDefaultValue']
			= $defaultValue;
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['tksfilterfield']   
			= $this->getBool($request->get('tksfilterfield'));
		$_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get('blockid')- 1]['tks_fields'][$fieldId]['tksrequired'] 	   
			= $this->getBool($request->get('mandatory'));
													
		$lbl = $_SESSION['tks_module_builder'][$token]['tks_blocks'][$request->get( 'blockid' ) - 1]['tks_fields'][$fieldId]['label'];
        $response = new Vtiger_Response();
        try
		{

            $response->setResult( array( 'success'=>true,'presence'=>$request->get( 'presence' ), 
										'mandatory'=>$this->getBool( $request->get( 'mandatory' ) ),
										'label'=>$lbl, 'tksfilterfield'=>$this->getBool( $request->get( 'tksfilterfield' ) ),
										'id' => $fieldId, 'blockid' => $request->get( 'blockid' ) ) );
        }
		catch( Exception $e ) 
		{
            $response->setError( $e->getCode(), $e->getMessage() );
			$log->debug("save(request array()) method.... ERROR OCCURED");
        }
		$log->debug("Exiting save(request array()) method....");
        $response->emit();
    }
	
	/**
	 * Function to delete field
	*/
    public function delete( Vtiger_Request $request ) 
	{
		global $log;
		$log->debug("Entering delete(request array() method....");
		$token = $request->get('token');
		if($token == '')
		{
			$response = new Vtiger_Response();
			$response->setError('502', vtranslate('LBL_FAILED_MODULE_INIT', $request->getModule(false)));
			$response->emit();
			$log->debug("Exiting delete(request array()) method.... INITIALIZAION SESSION ERROR");
			return;
		}
		
		$response = new Vtiger_Response();
        try
		{

		   $fieldId = $request->get( 'fieldid' );
		   $blockid = $request->get( 'blockid' );
		   if( $blockid != '' && $fieldId != '' )
		   {	
				$_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockid - 1]['tks_fields'][$fieldId]['deleted'] = 'true';
				$fld = $_SESSION['tks_module_builder'][$token]['tks_blocks'][$blockid - 1]['tks_fields'][$fieldId];
            	$response->setResult( array( 'success'=>true, 'label'=>$fld['label'], 'blockid' => $request->get( 'blockid' ), 'id' => $fieldId  ) );
		   }
		   else
		   {
		   		$response->setResult( array( 'success'=>false ) );
		   }	
        }
		catch( Exception $e ) 
		{
            $response->setError( $e->getCode(), $e->getMessage() );
			$log->debug(" delete(request array()) method.... ERROR OCCURED");
        }
		$log->debug("Exiting delete(request array()) method....");
        $response->emit();
    }

	/**
	 * Function to save the moved fields
	*/
    public function move( Vtiger_Request $request ) 
	{
		global $log;
		$log->debug("Entering move(request array()) method....");
		
		$token = $request->get('token');
		if($token == '')
		{
			$response = new Vtiger_Response();
			$response->setError('502', vtranslate('LBL_FAILED_MODULE_INIT', $request->getModule(false)));
			$response->emit();
			$log->debug("Exiting move(request array()) method.... INITIALIZAION SESSION ERROR");
			return;
		}
		
        $updatedFieldsList = $request->get( 'updatedFields' );

		$tempsession = $_SESSION['tks_module_builder'][$token]['tks_blocks'];
		
		foreach( $updatedFieldsList as $ky => $val )
		{
			foreach($tempsession as $key => $value)
			{
				if(is_array($value['tks_fields']))
				{
					foreach($value['tks_fields'] as $k => $v)
					{
						if($v['tkslabel'] == $val['lbl'])
						{
							$tempArray = array($v);

							unset($tempsession[$key]['tks_fields'][$k]);
							$tempsession[$key]['tks_fields'] = array_values($tempsession[$key]['tks_fields']);
							foreach($tempsession as $block => $bvalue)
							{
								if(is_array($bvalue))
								{
									if( ($val['block']) == $bvalue['block_id'] )
									{
										$tempArray[0]['id'] 		= $val['sequence']-1;
										$tempArray[0]['blockid'] 	= $val['block']-1;
										array_splice($tempsession[$block]['tks_fields'], $val['sequence']-1, 0, $tempArray);
									}
								}	
							}
						}
					}
				}
			}
		}

		$_SESSION['tks_module_builder'][$token]['tks_blocks'] = array_reverse($tempsession);
        $response = new Vtiger_Response();
		$response->setResult( array( 'success'=>true ) );
		$log->debug("Exiting move(request array()) method....");
        $response->emit();
    }
}