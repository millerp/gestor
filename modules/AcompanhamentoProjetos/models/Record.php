<?php
class AcompanhamentoProjetos_Record_Model extends Vtiger_Base_Model {

    static function findAllArray($status = false) {
        $db = PearDatabase::getInstance();
        if ($status) {
            $filtro = "AND pj.projectstatus = '$status'";
        } else {
            $filtro = '';
        }
        $query = '
                SELECT *
                FROM vtiger_project pj 
                INNER JOIN vtiger_projectcf pjcf
                ON pj.projectid = pjcf.projectid
                INNER JOIN vtiger_crmentity crm
                ON crm.crmid = pj.projectid
                WHERE crm.deleted = 0
                '.$filtro.'
                ORDER BY pj.projectname ASC
            ';
        //print($query);
        $rs = $db->run_query_allrecords($query);
        return $rs;
    }

    static function findAllStatus() {
        $db = PearDatabase::getInstance();
        $query = "SELECT *
                    FROM `vtiger_projectstatus`";
        return $db->run_query_allrecords($query);
    }

}
