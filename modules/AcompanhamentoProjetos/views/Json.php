<?php

class AcompanhamentoProjetos_Json_View extends Vtiger_Basic_View {

    public function checkPermission() {
        return false;
    }

    public function preProcessTplName() {
        return header('Content-Type: application/json');
    }

    public function postProcess() {
        return false;
    }

    public function process(Vtiger_Request $request) {
        $viewer = $this->getViewer($request);
        
        if (isset($_GET['status'])) {
            $status = $_GET['status'];
        } else {
            $status = false;
        }

        $projetos = AcompanhamentoProjetos_Record_Model::findAllArray($status);
        if (isset($_GET['tipo'])) {
            $tipo = intval($_GET['tipo']);
            switch ($tipo) {
                case 0: // Total
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas pela equipe Comercial', 'Total Horas Trabalhadas');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_783']), intval($projeto['cf_781']));
                    }
                    $nomes['titulo'] = 'Total';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 1: // Progresso
                    $nomes['data'][] = array('Projetos', 'Progresso');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), $projeto['progress']);
                    }
                    $nomes['titulo'] = 'Progresso';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 2: // Análise
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas em Análise', 'Horas Trabalhadas em Análise');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_805']), intval($projeto['cf_807']));
                    }
                    $nomes['titulo'] = 'Análise';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 3: // Front-end
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas em Front-end', 'Horas Trabalhas em Front-end');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_787']), intval($projeto['cf_773']));
                    }
                    $nomes['titulo'] = 'Front-End';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 4: // Back-end
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas em Back-end', 'Horas Trabalhas em Back-end');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_785']), intval($projeto['cf_775']));
                    }
                    $nomes['titulo'] = 'Back-End';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 5: // Teste
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas em Teste', 'Horas Trabalhas em Teste');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_789']), intval($projeto['cf_777']));
                    }
                    $nomes['titulo'] = 'Teste';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                case 6: // Gerencia
                    $nomes['data'][] = array('Projetos', 'Horas Planejadas em Gerencia', 'Horas Trabalhadas em Gerencia');
                    foreach ($projetos as $projeto) {
                        $nomes['data'][] = array(($projeto['projectname']), intval($projeto['cf_801']), intval($projeto['cf_803']));
                    }
                    $nomes['titulo'] = 'Gerencia';
                    $nomes['subtitulo'] = $status ? $status : '';
                    break;
                default:
                    break;
            }
        }
        $viewer->assign('projetos', $nomes);
        $viewer->view('Json.tpl', $request->getModule());
    }

}
