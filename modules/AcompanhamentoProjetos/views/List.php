<?php

class AcompanhamentoProjetos_List_View extends Vtiger_Index_View {

    public function process(Vtiger_Request $request) {
        $viewer = $this->getViewer($request);   
        $projetoStatus = AcompanhamentoProjetos_Record_Model::findAllStatus();
        $viewer->assign('projetoStatus', $projetoStatus);
        $viewer->view('List.tpl', $request->getModule());
    }

}
