google.load("visualization", "1.1", {packages: ["bar"]});
//*****************************************************************************//
//                                Total                                        //
//*****************************************************************************//
$.ajax({
    url: "index.php?module=AcompanhamentoProjetos&view=Json&tipo=0",
    type: "GET",
    dataType: 'json'
}).done(function (retorno) {
    //console.log(retorno);
    var data = new google.visualization.arrayToDataTable(retorno.data);

    var options = {
        chart: {
            title: retorno.titulo,
            subtitle: retorno.subtitulo
        },
        legend: {
            position: 'top',
            textStyle: {
                color: 'blue', fontSize: 16
            }
        },
        bars: 'horizontal'
    };

    var chart_total = new google.charts.Bar(document.getElementById('chart_total'));

    chart_total.draw(data, options);
});

$(".filtroProjetos").chosen();

$(".filtroProjetos").change(function () {
   var element = $("#myTab .active a");

    $.ajax({
        url: "index.php?module=AcompanhamentoProjetos&view=Json&tipo="+element.attr('data-tipo')+"&status=" + $(this).val(),
        type: "GET",
        dataType: 'json'
    }).done(function (retorno) {
        var data = new google.visualization.arrayToDataTable(retorno.data);

        var options = {
            chart: {
                title: retorno.titulo,
                subtitle: retorno.subtitulo
            },
            legend: {
                position: 'top',
                textStyle: {
                    color: 'blue', fontSize: 16
                }
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById(element.attr('data-chart')));

        chart.draw(data, options);
    });
});

$("#myTab li a").click(function () {
    var $this = $(this);
    //console.log($this.attr('data-chart'));
    $.ajax({
        url: "index.php?module=AcompanhamentoProjetos&view=Json&tipo=" + $this.attr('data-tipo'),
        type: "GET",
        dataType: 'json',
        beforeSend: function () {
            //console.log('depouis');
            $('#' + $this.attr('data-chart')).block({
                message: 'Processando...',
                css: {border: '1px solid #a00'}
            });
        },
        success: function () {
            $('div#' + $this.attr('data-chart')).unblock();
        }
    }).done(function (retorno) {
        var data = new google.visualization.arrayToDataTable(retorno.data);

        var options = {
            chart: {
                title: retorno.titulo,
                subtitle: retorno.subtitulo
            },
            legend: {
                position: 'top',
                textStyle: {
                    color: 'blue', fontSize: 16
                }
            },
            bars: 'horizontal'
        };

        var chart = new google.charts.Bar(document.getElementById($this.attr('data-chart')));

        chart.draw(data, options);
    });
});