<?php

/**
 * Modulo de Acompanhamento de Projetos
 *
 * @author      Miller P. Magalhães <miller@agencia110.com.br>
 * @copyright   2010 ~ 2015 Agência Cento e Dez Marketing Digital
 * @link        https://www.agencia110.com.br
 * @since       1.0
 *
 * **********************************************************************
 * *                            Alterações                              *
 * **********************************************************************
 */
include_once 'modules/Vtiger/CRMEntity.php';

class AcompanhamentoProjetos extends Vtiger_CRMEntity {

    var $search_fields = Array(
        /* Format: Field Label => Array(tablename, columnname) */
        // tablename should not have prefix 'vtiger_'
        'Name' => Array('projects', 'projectname'),
        'Assigned To' => Array('vtiger_crmentity', 'assigned_user_id'),
    );

    function __construct() {
        parent::__construct();
    }

    public function vtlib_handler($moduleName, $eventType) {
        if ($eventType == 'module.postinstall') {
            $this->_registerLinks($moduleName);
        } else if ($eventType == 'module.enabled') {
            $this->_registerLinks($moduleName);
        } else if ($eventType == 'module.disabled') {
            $this->_deregisterLinks($moduleName);
        }
    }

    protected function _registerLinks($moduleName) {
        $thisModuleInstance = Vtiger_Module::getInstance($moduleName);
        if ($thisModuleInstance) {
            $thisModuleInstance->addLink("HEADERSCRIPT", "ChartJS", "modules/AcompanhamentoProjetos/js/Chart.min.js");
            $thisModuleInstance->addLink("HEADERSCRIPT", "DefaultJS", "modules/AcompanhamentoProjetos/js/default.js");
        }
    }

    protected function _deregisterLinks($moduleName) {
        $thisModuleInstance = Vtiger_Module::getInstance($moduleName);
        if ($thisModuleInstance) {
            $thisModuleInstance->deleteLink("HEADERSCRIPT", "ChartJS", "modules/AcompanhamentoProjetos/js/Chart.min.js");
            $thisModuleInstance->deleteLink("HEADERSCRIPT", "DefaultJS", "modules/AcompanhamentoProjetos/js/default.js");
        }
    }

}
