<?php

/* +**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * ********************************************************************************** */
require_once('data/CRMEntity.php');
require_once('data/Tracker.php');

class ProjectTask extends CRMEntity {

    var $db, $log; // Used in class functions of CRMEntity
    var $table_name = 'vtiger_projecttask';
    var $table_index = 'projecttaskid';
    var $column_fields = Array();

    /** Indicator if this is a custom module or standard module */
    var $IsCustomModule = true;

    /**
     * Mandatory table for supporting custom fields.
     */
    var $customFieldTable = Array('vtiger_projecttaskcf', 'projecttaskid');

    /**
     * Mandatory for Saving, Include tables related to this module.
     */
    var $tab_name = Array('vtiger_crmentity', 'vtiger_projecttask', 'vtiger_projecttaskcf');

    /**
     * Mandatory for Saving, Include tablename and tablekey columnname here.
     */
    var $tab_name_index = Array(
        'vtiger_crmentity' => 'crmid',
        'vtiger_projecttask' => 'projecttaskid',
        'vtiger_projecttaskcf' => 'projecttaskid');

    /**
     * Mandatory for Listing (Related listview)
     */
    var $list_fields = Array(
        /* Format: Field Label => Array(tablename, columnname) */
        // tablename should not have prefix 'vtiger_'
        'Project Task Name' => Array('projecttask', 'projecttaskname'),
        'Start Date' => Array('projecttask', 'startdate'),
        'End Date' => Array('projecttask', 'enddate'),
        'Type' => Array('projecttask', 'projecttasktype'),
        'Progress' => Array('projecttask', 'projecttaskprogress'),
        'Assigned To' => Array('crmentity', 'smownerid')
    );
    var $list_fields_name = Array(
        /* Format: Field Label => fieldname */
        'Project Task Name' => 'projecttaskname',
        'Start Date' => 'startdate',
        'End Date' => 'enddate',
        'Type' => 'projecttasktype',
        'Progress' => 'projecttaskprogress',
        'Assigned To' => 'assigned_user_id'
    );
    // Make the field link to detail view from list view (Fieldname)
    var $list_link_field = 'projecttaskname';
    // For Popup listview and UI type support
    var $search_fields = Array(
        /* Format: Field Label => Array(tablename, columnname) */
        // tablename should not have prefix 'vtiger_'
        'Project Task Name' => Array('projecttask', 'projecttaskname'),
        'Start Date' => Array('projecttask', 'startdate'),
        'Type' => Array('projecttask', 'projecttasktype'),
        'Assigned To' => Array('crmentity', 'smownerid')
    );
    var $search_fields_name = Array(
        /* Format: Field Label => fieldname */
        'Project Task Name' => 'projecttaskname',
        'Start Date' => 'startdate',
        'Type' => 'projecttasktype',
        'Assigned To' => 'assigned_user_id'
    );
    // For Popup window record selection
    var $popup_fields = Array('projecttaskname');
    // Placeholder for sort fields - All the fields will be initialized for Sorting through initSortFields
    var $sortby_fields = Array();
    // For Alphabetical search
    var $def_basicsearch_col = 'projecttaskname';
    // Column value to use on detail view record text display
    var $def_detailview_recname = 'projecttaskname';
    // Required Information for enabling Import feature
    var $required_fields = Array('projecttaskname' => 1);
    // Callback function list during Importing
    var $special_functions = Array('set_import_assigned_user');
    var $default_order_by = 'projecttaskname';
    var $default_sort_order = 'ASC';
    // Used when enabling/disabling the mandatory fields for the module.
    // Refers to vtiger_field.fieldname values.
    var $mandatory_fields = Array('createdtime', 'modifiedtime', 'projecttaskname', 'projectid', 'assigned_user_id');

    function __construct() {
        global $log, $currentModule;
        $this->column_fields = getColumnFields(get_class($this));
        $this->db = PearDatabase::getInstance();
        $this->log = $log;
    }

    function save_module($module) {
        
    }

    /**
     * Return query to use based on given modulename, fieldname
     * Useful to handle specific case handling for Popup
     */
    function getQueryByModuleField($module, $fieldname, $srcrecord) {
        // $srcrecord could be empty
    }

    /**
     * Get list view query (send more WHERE clause condition if required)
     */
    function getListQuery($module, $where = '') {
        $query = "SELECT vtiger_crmentity.*, $this->table_name.*";

        // Keep track of tables joined to avoid duplicates
        $joinedTables = array();

        // Select Custom Field Table Columns if present
        if (!empty($this->customFieldTable))
            $query .= ", " . $this->customFieldTable[0] . ".* ";

        $query .= " FROM $this->table_name";

        $query .= "	INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = $this->table_name.$this->table_index";

        $joinedTables[] = $this->table_name;
        $joinedTables[] = 'vtiger_crmentity';

        // Consider custom table join as well.
        if (!empty($this->customFieldTable)) {
            $query .= " INNER JOIN " . $this->customFieldTable[0] . " ON " . $this->customFieldTable[0] . '.' . $this->customFieldTable[1] .
                    " = $this->table_name.$this->table_index";
            $joinedTables[] = $this->customFieldTable[0];
        }
        $query .= " LEFT JOIN vtiger_users ON vtiger_users.id = vtiger_crmentity.smownerid";
        $query .= " LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid";

        $joinedTables[] = 'vtiger_users';
        $joinedTables[] = 'vtiger_groups';

        $linkedModulesQuery = $this->db->pquery("SELECT distinct fieldname, columnname, relmodule FROM vtiger_field" .
                " INNER JOIN vtiger_fieldmodulerel ON vtiger_fieldmodulerel.fieldid = vtiger_field.fieldid" .
                " WHERE uitype='10' AND vtiger_fieldmodulerel.module=?", array($module));
        $linkedFieldsCount = $this->db->num_rows($linkedModulesQuery);

        for ($i = 0; $i < $linkedFieldsCount; $i++) {
            $related_module = $this->db->query_result($linkedModulesQuery, $i, 'relmodule');
            $fieldname = $this->db->query_result($linkedModulesQuery, $i, 'fieldname');
            $columnname = $this->db->query_result($linkedModulesQuery, $i, 'columnname');

            $other = CRMEntity::getInstance($related_module);
            vtlib_setup_modulevars($related_module, $other);

            if (!in_array($other->table_name, $joinedTables)) {
                $query .= " LEFT JOIN $other->table_name ON $other->table_name.$other->table_index = $this->table_name.$columnname";
                $joinedTables[] = $other->table_name;
            }
        }

        global $current_user;
        $query .= $this->getNonAdminAccessControlQuery($module, $current_user);
        $query .= "	WHERE vtiger_crmentity.deleted = 0 " . $where;
        return $query;
    }

    /**
     * Apply security restriction (sharing privilege) query part for List view.
     */
    function getListViewSecurityParameter($module) {
        global $current_user;
        require('user_privileges/user_privileges_' . $current_user->id . '.php');
        require('user_privileges/sharing_privileges_' . $current_user->id . '.php');

        $sec_query = '';
        $tabid = getTabid($module);

        if ($is_admin == false && $profileGlobalPermission[1] == 1 && $profileGlobalPermission[2] == 1 && $defaultOrgSharingPermission[$tabid] == 3) {

            $sec_query .= " AND (vtiger_crmentity.smownerid in($current_user->id) OR vtiger_crmentity.smownerid IN
                    (
                        SELECT vtiger_user2role.userid FROM vtiger_user2role
                        INNER JOIN vtiger_users ON vtiger_users.id=vtiger_user2role.userid
                        INNER JOIN vtiger_role ON vtiger_role.roleid=vtiger_user2role.roleid
                        WHERE vtiger_role.parentrole LIKE '" . $current_user_parent_role_seq . "::%'
                    )
                    OR vtiger_crmentity.smownerid IN
                    (
                        SELECT shareduserid FROM vtiger_tmp_read_user_sharing_per
                        WHERE userid=" . $current_user->id . " AND tabid=" . $tabid . "
                    )
                    OR
                        (";

            // Build the query based on the group association of current user.
            if (sizeof($current_user_groups) > 0) {
                $sec_query .= " vtiger_groups.groupid IN (" . implode(",", $current_user_groups) . ") OR ";
            }
            $sec_query .= " vtiger_groups.groupid IN
                        (
                            SELECT vtiger_tmp_read_group_sharing_per.sharedgroupid
                            FROM vtiger_tmp_read_group_sharing_per
                            WHERE userid=" . $current_user->id . " and tabid=" . $tabid . "
                        )";
            $sec_query .= ")
                )";
        }
        return $sec_query;
    }

    /**
     * Create query to export the records.
     */
    function create_export_query($where) {
        global $current_user;

        include("include/utils/ExportUtils.php");

        //To get the Permitted fields query and the permitted fields list
        $sql = getPermittedFieldsQuery('ProjectTask', "detail_view");

        $fields_list = getFieldsListFromQuery($sql);

        $query = "SELECT $fields_list, vtiger_users.user_name AS user_name
					FROM vtiger_crmentity INNER JOIN $this->table_name ON vtiger_crmentity.crmid=$this->table_name.$this->table_index";

        if (!empty($this->customFieldTable)) {
            $query .= " INNER JOIN " . $this->customFieldTable[0] . " ON " . $this->customFieldTable[0] . '.' . $this->customFieldTable[1] .
                    " = $this->table_name.$this->table_index";
        }

        $query .= " LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid";
        $query .= " LEFT JOIN vtiger_users ON vtiger_crmentity.smownerid = vtiger_users.id and vtiger_users.status='Active'";

        $linkedModulesQuery = $this->db->pquery("SELECT distinct fieldname, columnname, relmodule FROM vtiger_field" .
                " INNER JOIN vtiger_fieldmodulerel ON vtiger_fieldmodulerel.fieldid = vtiger_field.fieldid" .
                " WHERE uitype='10' AND vtiger_fieldmodulerel.module=?", array($thismodule));
        $linkedFieldsCount = $this->db->num_rows($linkedModulesQuery);

        for ($i = 0; $i < $linkedFieldsCount; $i++) {
            $related_module = $this->db->query_result($linkedModulesQuery, $i, 'relmodule');
            $fieldname = $this->db->query_result($linkedModulesQuery, $i, 'fieldname');
            $columnname = $this->db->query_result($linkedModulesQuery, $i, 'columnname');

            $other = CRMEntity::getInstance($related_module);
            vtlib_setup_modulevars($related_module, $other);

            $query .= " LEFT JOIN $other->table_name ON $other->table_name.$other->table_index = $this->table_name.$columnname";
        }

        $query .= $this->getNonAdminAccessControlQuery($thismodule, $current_user);
        $where_auto = " vtiger_crmentity.deleted=0";

        if ($where != '')
            $query .= " WHERE ($where) AND $where_auto";
        else
            $query .= " WHERE $where_auto";

        return $query;
    }

    /**
     * Transform the value while exporting
     */
    function transform_export_value($key, $value) {
        return parent::transform_export_value($key, $value);
    }

    /**
     * Function which will give the basic query to find duplicates
     */
    function getDuplicatesQuery($module, $table_cols, $field_values, $ui_type_arr, $select_cols = '') {
        $select_clause = "SELECT " . $this->table_name . "." . $this->table_index . " AS recordid, vtiger_users_last_import.deleted," . $table_cols;

        // Select Custom Field Table Columns if present
        if (isset($this->customFieldTable))
            $query .= ", " . $this->customFieldTable[0] . ".* ";

        $from_clause = " FROM $this->table_name";

        $from_clause .= "	INNER JOIN vtiger_crmentity ON vtiger_crmentity.crmid = $this->table_name.$this->table_index";

        // Consider custom table join as well.
        if (isset($this->customFieldTable)) {
            $from_clause .= " INNER JOIN " . $this->customFieldTable[0] . " ON " . $this->customFieldTable[0] . '.' . $this->customFieldTable[1] .
                    " = $this->table_name.$this->table_index";
        }
        $from_clause .= " LEFT JOIN vtiger_users ON vtiger_users.id = vtiger_crmentity.smownerid
						LEFT JOIN vtiger_groups ON vtiger_groups.groupid = vtiger_crmentity.smownerid";

        $where_clause = "	WHERE vtiger_crmentity.deleted = 0";
        $where_clause .= $this->getListViewSecurityParameter($module);

        if (isset($select_cols) && trim($select_cols) != '') {
            $sub_query = "SELECT $select_cols FROM  $this->table_name AS t " .
                    " INNER JOIN vtiger_crmentity AS crm ON crm.crmid = t." . $this->table_index;
            // Consider custom table join as well.
            if (isset($this->customFieldTable)) {
                $sub_query .= " LEFT JOIN " . $this->customFieldTable[0] . " tcf ON tcf." . $this->customFieldTable[1] . " = t.$this->table_index";
            }
            $sub_query .= " WHERE crm.deleted=0 GROUP BY $select_cols HAVING COUNT(*)>1";
        } else {
            $sub_query = "SELECT $table_cols $from_clause $where_clause GROUP BY $table_cols HAVING COUNT(*)>1";
        }


        $query = $select_clause . $from_clause .
                " LEFT JOIN vtiger_users_last_import ON vtiger_users_last_import.bean_id=" . $this->table_name . "." . $this->table_index .
                " INNER JOIN (" . $sub_query . ") AS temp ON " . get_on_clause($field_values, $ui_type_arr, $module) .
                $where_clause .
                " ORDER BY $table_cols," . $this->table_name . "." . $this->table_index . " ASC";

        return $query;
    }

    /**
     * Invoked when special actions are performed on the module.
     * @param String Module name
     * @param String Event Type (module.postinstall, module.disabled, module.enabled, module.preuninstall)
     */
    function vtlib_handler($modulename, $event_type) {
        global $adb;
        if ($event_type == 'module.postinstall') {
            $projectTaskResult = $adb->pquery('SELECT tabid FROM vtiger_tab WHERE name=?', array('ProjectTask'));
            $projecttaskTabid = $adb->query_result($projectTaskResult, 0, 'tabid');

            // Mark the module as Standard module
            $adb->pquery('UPDATE vtiger_tab SET customized=0 WHERE name=?', array($modulename));

            if (getTabid('CustomerPortal')) {
                $checkAlreadyExists = $adb->pquery('SELECT 1 FROM vtiger_customerportal_tabs WHERE tabid=?', array($projecttaskTabid));
                if ($checkAlreadyExists && $adb->num_rows($checkAlreadyExists) < 1) {
                    $maxSequenceQuery = $adb->query("SELECT max(sequence) as maxsequence FROM vtiger_customerportal_tabs");
                    $maxSequence = $adb->query_result($maxSequenceQuery, 0, 'maxsequence');
                    $nextSequence = $maxSequence + 1;
                    $adb->query("INSERT INTO vtiger_customerportal_tabs(tabid,visible,sequence) VALUES ($projecttaskTabid,1,$nextSequence)");
                    $adb->query("INSERT INTO vtiger_customerportal_prefs(tabid,prefkey,prefvalue) VALUES ($projecttaskTabid,'showrelatedinfo',1)");
                }
            }

            $modcommentsModuleInstance = Vtiger_Module::getInstance('ModComments');
            if ($modcommentsModuleInstance && file_exists('modules/ModComments/ModComments.php')) {
                include_once 'modules/ModComments/ModComments.php';
                if (class_exists('ModComments'))
                    ModComments::addWidgetTo(array('ProjectTask'));
            }

            $result = $adb->pquery("SELECT 1 FROM vtiger_modentity_num WHERE semodule = ? AND active = 1", array($modulename));
            if (!($adb->num_rows($result))) {
                //Initialize module sequence for the module
                $adb->pquery("INSERT INTO vtiger_modentity_num values(?,?,?,?,?,?)", array($adb->getUniqueId("vtiger_modentity_num"), $modulename, 'PT', 1, 1, 1));
            }
        } else if ($event_type == 'module.disabled') {
            // TODO Handle actions when this module is disabled.
        } else if ($event_type == 'module.enabled') {
            // TODO Handle actions when this module is enabled.
        } else if ($event_type == 'module.preuninstall') {
            // TODO Handle actions when this module is about to be deleted.
        } else if ($event_type == 'module.preupdate') {
            // TODO Handle actions before this module is updated.
        } else if ($event_type == 'module.postupdate') {

            $modcommentsModuleInstance = Vtiger_Module::getInstance('ModComments');
            if ($modcommentsModuleInstance && file_exists('modules/ModComments/ModComments.php')) {
                include_once 'modules/ModComments/ModComments.php';
                if (class_exists('ModComments'))
                    ModComments::addWidgetTo(array('ProjectTask'));
            }

            $result = $adb->pquery("SELECT 1 FROM vtiger_modentity_num WHERE semodule = ? AND active = 1", array($modulename));
            if (!($adb->num_rows($result))) {
                //Initialize module sequence for the module
                $adb->pquery("INSERT INTO vtiger_modentity_num values(?,?,?,?,?,?)", array($adb->getUniqueId("vtiger_modentity_num"), $modulename, 'PT', 1, 1, 1));
            }
        }
    }

    /**
     * Function to check the module active and user action permissions before showing as link in other modules
     * like in more actions of detail view(Projects).
     */
    static function isLinkPermitted($linkData) {
        $moduleName = "ProjectTask";
        if (vtlib_isModuleActive($moduleName) && isPermitted($moduleName, 'EditView') == 'yes') {
            return true;
        }
        return false;
    }

    public function calcHoursDiffDates($date1, $date2) {

        $t1 = strtotime($date2);
        $t2 = strtotime($date1);
        $diff = $t1 - $t2;
        $hours = $diff / ( 60 * 60 );

        //$hours = $hours + ($diff->days * 24);
        return $hours;
    }

    public function cronUpdateProjectTaskAndProjectRelated() {
        //error_reporting(E_ALL);
        GLOBAL $adb;
        require 'include/vendor/autoload.php';
        require_once 'modules/Project/Project.php';

        $projetosObj = new Project();
        $queryProjetos = $projetosObj->getListQuery('Project');
        $queryTarefas = $this->getListQuery('ProjectTask');


        $queryEventos = "SELECT 
                            vtiger_crmentity.*,
                            vtiger_activity.* 
                          FROM
                            vtiger_activity
                            INNER JOIN vtiger_crmentity 
                              ON vtiger_crmentity.crmid = vtiger_activity.activityid 
                          WHERE vtiger_activity.eventstatus = 'Held' 
                            AND vtiger_activity.projecttaskid IS NOT NULL
                            AND vtiger_activity.status IS NULL 
                            AND vtiger_crmentity.deleted = 0";
        $eventos = $adb->run_query_allrecords($queryEventos);


        // Eventos
        foreach ($eventos as $evento) {
            $taskid = $evento['projecttaskid'];
            $taskhoras = $this->calcHoursDiffDates($evento['date_start'] . ' ' . $evento['time_start'], $evento['due_date'] . ' ' . $evento['time_end']);
            $taskQuery = $this->getListQuery('ProjectTask', 'AND vtiger_projecttask.projecttaskid =' . $taskid);
            $taskHorasAnterior = $adb->run_query_record('SELECT vtiger_projecttaskcf.cf_757 FROM vtiger_projecttaskcf WHERE projecttaskid = ' . $taskid)['cf_757'];
            $totalHoras = $taskHorasAnterior + $taskhoras;
            $resultTaskUpdate = $adb->pquery("UPDATE `vtiger_projecttaskcf` SET cf_757 = '" . $totalHoras . "' WHERE `projecttaskid` = '" . $taskid . "'");
            $resultTaskUpdate = $adb->pquery("UPDATE `vtiger_activity` SET status = 'Finalizado' WHERE `activityid` = '" . $evento['activityid'] . "'");
        }


        // Tarefas
        $tarefas = $adb->run_query_allrecords($queryTarefas);

        foreach ($tarefas as $tarefa) {
            switch ($tarefa['projecttasktype']) {

                case 'Analise': // Analise
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_807, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_807'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_807'];
                        }
                        $TotalHorasAnalise[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                case 'Design': // Design
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_765, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_765'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_765'];
                        }
                        $TotalHorasDesign[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                case 'Frontend': // Frontend
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_773, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_773'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_773'];
                        }
                        $TotalHorasFrontend[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                case 'Backend': // Backend
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_775, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_775'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_775'];
                        }
                        $TotalHorasBackend[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                case 'Teste': // Teste
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_777, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_777'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_777'];
                        }
                        $TotalHorasTeste[$tarefa['projectid']] += $tarefa['cf_777'];
                    }
                    break;

                case 'Conteudo': // Conteudo
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_767, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_767'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_767'];
                        }
                        $TotalHorasConteudo[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                case 'Gerencia': // Gerencia
                    if (!empty($tarefa['projectid'])) {
                        $HorasAnterior = $adb->run_query_record('SELECT vtiger_projectcf.cf_803, vtiger_projectcf.horas_anterior FROM vtiger_projectcf WHERE projectid = ' . $tarefa['projectid']);
                        if ($tarefa['horas_anterior'] !== $tarefa['cf_757']) {
                            $HorasTotal = ($HorasAnterior['cf_803'] + $tarefa['cf_757'] - $tarefa['horas_anterior']);
                            $adb->pquery("UPDATE `vtiger_projecttaskcf` SET horas_anterior = '" . $tarefa['cf_757'] . "' WHERE `projecttaskid` = '" . $tarefa['projecttaskid'] . "'");
                        } else {
                            $HorasTotal = $HorasAnterior['cf_803'];
                        }
                        $TotalHorasGerencia[$tarefa['projectid']] += $tarefa['cf_757'];
                    }
                    break;

                default:
                    break;
            }
        }

        // Analise
        foreach ($TotalHorasAnalise as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_807 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // Design
        foreach ($TotalHorasDesign as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_765 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // FrontEnd
        foreach ($TotalHorasFrontend as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_773 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // BackEnd
        foreach ($TotalHorasBackend as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_775 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // Teste
        foreach ($TotalHorasTeste as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_777 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // Conteudo
        foreach ($TotalHorasConteudo as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_767 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }

        // Gerencia
        foreach ($TotalHorasGerencia as $projectid => $total) {
            $adb->pquery("UPDATE `vtiger_projectcf` SET cf_803 = '" . $total . "' WHERE `projectid` = '" . $projectid . "'");
        }
    }

}

?>
