<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H8';

$current_user_parent_role_seq='H1::H2::H3::H4::H8';

$current_user_profiles=array(8,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'2'=>1,'4'=>0,'6'=>0,'7'=>1,'8'=>0,'9'=>0,'10'=>0,'13'=>0,'14'=>1,'15'=>0,'16'=>0,'18'=>1,'19'=>1,'20'=>1,'21'=>1,'22'=>1,'23'=>1,'24'=>0,'25'=>0,'26'=>1,'30'=>0,'33'=>0,'34'=>0,'37'=>0,'38'=>0,'39'=>0,'40'=>0,'41'=>0,'42'=>0,'43'=>0,'45'=>0,'46'=>0,'47'=>0,'28'=>0,'3'=>0,);

$profileActionPermission=array(2=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,10=>0,),4=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,8=>0,10=>0,),6=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,8=>0,10=>0,),7=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,8=>0,9=>0,10=>0,),8=>array(0=>0,1=>0,2=>0,4=>0,6=>0,),9=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,),13=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,8=>0,10=>0,),14=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,10=>0,),15=>array(0=>0,1=>0,2=>0,4=>0,),16=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,),18=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,10=>0,),20=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),21=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),22=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),23=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),30=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,10=>0,),34=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,10=>0,),37=>array(0=>0,1=>0,2=>0,4=>0,),38=>array(0=>0,1=>0,2=>0,4=>0,),41=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,10=>0,),42=>array(0=>1,1=>1,2=>1,4=>0,5=>0,6=>0,10=>0,),43=>array(0=>0,1=>0,2=>0,4=>0,5=>0,6=>0,10=>0,),46=>array(0=>0,1=>0,2=>0,4=>0,),);

$current_user_groups=array(3,4,);

$subordinate_roles=array();

$parent_roles=array('H1','H2','H3','H4',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'daniel.mota','is_admin'=>'off','user_password'=>'$1$da000000$t8r5x2Z7xNKp4BFmdpzq6/','confirm_password'=>'$1$da000000$t8r5x2Z7xNKp4BFmdpzq6/','first_name'=>'Daniel','last_name'=>'Mota','roleid'=>'H8','email1'=>'daniel@agencia110.com.br','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'24','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'9U8dJPo3xxRXlD6G','time_zone'=>'America/Sao_Paulo','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>',','currency_grouping_separator'=>'.','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'softed','language'=>'pt_br','reminder_interval'=>'5 Minutes','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'medium','ccurrency_name'=>'','currency_code'=>'BRL','currency_symbol'=>'R$','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','currency_name'=>'Brazil, Reais','id'=>'13');
?>