﻿<div class="container-fluid">
    <br/>
    <div class="row-fluid">
        <div class="span4">
            <label for="filtroProjetos" class="label">Filtrar por Status</label>
            <select id="filtroProjetos" class="span6 filtroProjetos">
                <option></option>
                {foreach key=key item=item from=$projetoStatus}
                    <option value='{$item['projectstatus']}'>{$item['projectstatus']}</option>
                {/foreach}
            </select>
        </div>
    </div>

    <div class="row-fluid">
        <ul class="nav nav-tabs" id="myTab">
            <li class="active"><a href="#total" data-tipo="0" data-chart="chart_total">Total</a></li>
            <li><a href="#progresso" data-tipo="1" data-chart="chart_progresso">Progresso</a></li>
            <li><a href="#analise" data-tipo="2" data-chart="chart_analise">Análise</a></li>
            <li><a href="#frontend" data-tipo="3" data-chart="chart_frontend">Front-end</a></li>
            <li><a href="#backend" data-tipo="4" data-chart="chart_backend">Back-end</a></li>
            <li><a href="#teste" data-tipo="5" data-chart="chart_teste">Teste</a></li>
            <li><a href="#gerencia" data-tipo="6" data-chart="chart_gerencia">Gerencia</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="total">
                <div id="chart_total" style="width: 100%; height: 700px;"></div>                  
            </div>
            <div class="tab-pane" id="progresso">
                <div id="chart_progresso" style="width: 100%; height: 700px;"></div>                
            </div>
            <div class="tab-pane" id="analise">
                <div id="chart_analise" style="width: 100%; height: 700px;"></div>                
            </div>
            <div class="tab-pane" id="frontend">
                <div id="chart_frontend" style="width: 100%; height: 700px;"></div>
            </div>
            <div class="tab-pane" id="backend">
                <div id="chart_backend" style="width: 100%; height: 700px;"></div>
            </div>
            <div class="tab-pane" id="teste">
                <div id="chart_teste" style="width: 100%; height: 700px;"></div>
            </div>
            <div class="tab-pane" id="gerencia">
                <div id="chart_gerencia" style="width: 100%; height: 700px;"></div>
            </div>
        </div>

        <script>
            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
        </script>
    </div>
    {*
    <div class="row-fluid">
    <div class="span6">
    <div class="alert alert-info" style="text-align: center;"><h3>Total(Horas)</h3></div>
    <canvas id="myChart" width="750" height="500"></canvas>
    </div>

    <div class="span6">
    <div class="alert alert-info" style="text-align: center;"><h3>Progresso(%)</h3></div>
    <canvas id="myChart2" width="750" height="500"></canvas>
    </div>
    </div>

    <div class="row-fluid">
    <div class="span6">            
    <div class="alert alert-info" style="text-align: center;"><h3>Análise(Horas)</h3></div>
    <canvas id="myChart3" width="750" height="500"></canvas>
    </div>
    <div class="span6">
    <div class="alert alert-info" style="text-align: center;"><h3>Front-End(Horas)</h3></div>
    <canvas id="myChart4" width="750" height="500"></canvas>
    </div>
    </div>

    <div class="row-fluid">
    <div class="span6">            
    <div class="alert alert-info" style="text-align: center;"><h3>Back-End(Horas)</h3></div>
    <canvas id="myChart5" width="750" height="500"></canvas>
    </div>
    <div class="span6">
    <div class="alert alert-info" style="text-align: center;"><h3>Teste(Horas)</h3></div>
    <canvas id="myChart6" width="750" height="500"></canvas>
    </div>
    </div>

    <div class="row-fluid">
    <div class="span6">
    <div class="alert alert-info" style="text-align: center;"><h3>Gerencia(Horas)</h3></div>
    <canvas id="myChart7" width="750" height="500"></canvas>
    </div>
    </div>

    *}
    <div class="clearfix">
        <br/>
        <br/>
        <br/>
    </div>
</div>